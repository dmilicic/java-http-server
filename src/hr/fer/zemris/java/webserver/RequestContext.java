package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Request context class handles all data transfer from a server to a client. Generates a HTTP header for a client and all of the necessary data
 * the client requests including cookies.
 * @author Dario Miličić
 *
 */
public class RequestContext {
	
	private OutputStream outputStream;
	private Charset charset;
	
	private String encoding = "UTF-8";
	private int statusCode = 200;
	private String statusText = "OK";
	private String mimeType = "text/html";
	
	private Map<String, String> parameters;
	private Map<String, String> persistentParameters;
	private List<RCCookie> outputCookies;
	
	private boolean headerGenerated = false;
	
	/**
	 * Default constructor for this request context. 
	 * @param outputStream output stream to send the data to
	 * @param parameters parameters to be written
	 * @param persistentParameters persistent parameters used for smart script execution
	 * @param outputCookies cookies to be sent to client
	 */
	public RequestContext(OutputStream outputStream,
			Map<String, String> parameters,
			Map<String, String> persistentParameters,
			List<RCCookie> outputCookies) {
		super();
		
		if(outputStream == null) throw new NullPointerException();
		if(parameters == null) {
			this.parameters = new HashMap<>();
		} else {
			this.parameters = parameters;
		}
		if(persistentParameters == null) {
			this.persistentParameters = new HashMap<>();
		} else {
			this.persistentParameters = persistentParameters;
		}
		if(outputCookies == null) {
			this.outputCookies = new ArrayList<>();
		} else {
			this.outputCookies = outputCookies;
		}
		
		this.outputStream = outputStream;
	}
	
	/**
	 * Generates a header that is sent to client. Since multiple headers are not allowed, this method will only be called once during
	 * client data transfer
	 * @throws IOException
	 */
	private void generateHeader() throws IOException {
		headerGenerated = true;
		charset = Charset.forName(encoding);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("HTTP/1.1 " + Integer.toString(getStatusCode()) + " " + getStatusText() + "\r\n");
		sb.append("Content-Type: " + mimeType);
		if(mimeType.substring(0,  4).equals("text")) {
			sb.append("; charset=" + encoding);
		}
		
		for(RCCookie c : outputCookies) {
			sb.append("\r\nSet-Cookie: "+c.name + "=" + "\"" + c.value + "\"");
			if(c.domain != null) {
				sb.append("; Domain="+c.domain);
			}
			if(c.path != null) {
				sb.append("; Path="+c.path);
			}
			if(c.maxAge != null) {
				sb.append("; Max-Age="+c.maxAge.toString());
			}
		}
		
		sb.append("\r\n\r\n");
	
		outputStream.write(sb.toString().getBytes(charset));
	}
	
	/**
	 * Writes an array of bytes to the request context output stream
	 * @param data an array of bytes to be sent
	 * @return returns a reference to this class
	 * @throws IOException
	 */
	public RequestContext write(byte[] data) throws IOException {
		if(!headerGenerated) {
			generateHeader();
		}
		
		outputStream.write(data);
		return this;
	}
	
	/**
	 * Writes a string of text to the request context output stream
	 * @param text string to be written
	 * @return returns a reference to this class
	 * @throws IOException
	 */
	public RequestContext write(String text) throws IOException {
		if(!headerGenerated) {
			generateHeader();
		}
		
		outputStream.write(text.getBytes(charset));
		return this;
	}

	/**
	 * Gets the value of the parameter for a given key from the parameters map from this request context
	 * @param name key for which the value is returned
	 * @return returns the value from the key-value pair 
	 */
	public String getParameter(String name) {
		return parameters.get(name);
	}
	
	/**
	 * Gets the set of all the keys in the parameters map
	 * @return returns a set of all key names
	 */
	public Set<String> getParemeterNames() {
		return parameters.keySet();
	}
	
	/**
	 * Gets the value of the persistent parameters map for a given key from this request context
	 * @param name key for which the value is returned
	 * @return returns the value from the key-value pair
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(name);
	}
	
	/**
	 * Gets the set of all the keys in the persistent parameters map
	 * @return returns a set of all key names
	 */
	public Set<String> getPersistentParameterNames() {
		return persistentParameters.keySet();
	}
	
	/**
	 * Sets a value in the persistent parameters map for a given key as a key-value pair
	 * @param name a key for which the value is added
	 * @param value value to be added
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(name, value);
	}

	/**
	 * Gets the encoding of this request context. If encoding is not set by a setter method, the default encoding is UTF-8.
	 * @return
	 */
	public String getEncoding() {
		return encoding;
	}

	/**
	 * Sets the encoding for this request context. If the header has already been generated then it is illegal to change the encoding so a runtime exception is thrown
	 * if this method is called.
	 * @param encoding encoding to be set
	 */
	public void setEncoding(String encoding) {
		if(headerGenerated) throw new RuntimeException();
		this.encoding = encoding;
	}

	/**
	 * Gets the status code for this request context, used for generating a header.
	 * @return returns the status code
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the status code for this request context. If the header has already been generated then it is illegal to change the status code so a runtime exception is thrown
	 * if this method is called
	 * @param statusCode status code to be set
	 */
	public void setStatusCode(int statusCode) {
		if(headerGenerated) throw new RuntimeException();
		this.statusCode = statusCode;
	}

	/**
	 * Gets the status text for this request context, used for generating a header.
	 * @return returns the status text
	 */
	public String getStatusText() {
		return statusText;
	}

	/**
	 * Sets the status text for this request context. If the header has already been generated then it is illegal to change the status text so a runtime exception is thrown
	 * if this method is called
	 * @param statusText status text to be set
	 */
	public void setStatusText(String statusText) {
		if(headerGenerated) throw new RuntimeException();
		this.statusText = statusText;
	}

	/**
	 * Gets the mime type for this request context, used for generating a header.
	 * @return returns the mime type string
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type for this request context. If the header has already been generated then it is illegal to change the mime type so a runtime exception is thrown
	 * if this method is called
	 * @param mimeType
	 */
	public void setMimeType(String mimeType) {
		if(headerGenerated) throw new RuntimeException();
		this.mimeType = mimeType;
	}
	
	/**
	 * Adds a cookie to the list of output cookies
	 * @param cookie cookie to be added
	 */
	public void addRCCookie(RCCookie cookie) {
		outputCookies.add(cookie);
	}

	/**
	 * A cookie class for storing cookies
	 * @author Dario Miličić
	 *
	 */
	public static class RCCookie {
		private String name;
		private String value;
		private String domain;
		private String path;
		private Integer maxAge;
		
		/**
		 * Default constructor for creating a cookie
		 * @param name name of the cookie
		 * @param value value of the cookie
		 * @param maxAge lifespan of the cookie
		 * @param domain domain for which the cookie is valid
		 * @param path path for which the requests for this cookie are valid
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain,
				String path) {
			super();
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}
		
		/**
		 * Gets the name of the cookie
		 * @return returns a name of this cookie
		 */
		public String getName() {
			return name;
		}
		
		/**
		 * Gets the value of the cookie
		 * @return returns the value of this cookie
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the domain for which this cookie is created
		 * @return returns the domain of this cookie
		 */
		public String getDomain() {
			return domain;
		}
		
		/**
		 * Gets the server directory path for which this cookie is valid
		 * @return returns the path string for this cookie
		 */
		public String getPath() {
			return path;
		}
		
		/**
		 * Gets the lifespan of this cookie in seconds, the maximum age this cookie will be valid.
		 * @return returns the maximum age value
		 */
		public int getMaxAge() {
			return maxAge;
		}
	}
}
