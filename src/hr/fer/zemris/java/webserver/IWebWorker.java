package hr.fer.zemris.java.webserver;

/**
 * An interface for processing worker requests
 * @author Dario Miličić
 *
 */
public interface IWebWorker {
	
	/**
	 * Processes the request of a particular server worker that implements this method
	 * @param context request context to be processed by a worker
	 */
	public void processRequest(RequestContext context);
}	
