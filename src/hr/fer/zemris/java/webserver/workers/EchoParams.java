package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.util.Set;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class that implements process request method from IWebWorker interface and is used to print client parameters in a html table
 * @author Dario Miličić
 *
 */
public class EchoParams implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		Set<String>names = context.getParemeterNames();
		try {
			context.write("<html><body><table border=\"1\"");
			for(String s : names) {
				context.write("<tr><td>" + s + "</td><td>" + context.getParameter(s) + "</td></tr>");
			}
			context.write("</table></body></html>");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
