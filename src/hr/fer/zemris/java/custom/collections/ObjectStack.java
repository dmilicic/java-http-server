package hr.fer.zemris.java.custom.collections;

/**
 * 
 * @author Dario Miličić
 *
 */

public class ObjectStack extends ArrayBackedIndexedCollection {
	
	private int stackPointer;
	
	/**
	 * Default constructor
	 */
	
	public ObjectStack() {
		super();
		stackPointer = 0;
	}
	
	/**
	 * Constructor with initial stack capacity
	 * @param initialCapacity sets the initial capacity of the object stack
	 */
	
	public ObjectStack(int initialCapacity) {
		super(initialCapacity);
		stackPointer = 0;
	}
	
	/**
	 * Checks if the stack is empty
	 * @return returns true if the stack is empty or false if the stack has at least one element
	 */
	
	public boolean isEmpty() {
		return super.isEmpty();
	}
	
	/**
	 * Puts an object on top of the stack
	 * @param value Object to be put on top of the stack
	 * @throws IllegalArgumentException argument must be a valid object and not null
	 */
	
	public void push(Object value) throws IllegalArgumentException {
		if( value == null ) throw new IllegalArgumentException();
		super.add(value);
		stackPointer++;
	}
	
	/**
	 * Returns and erases the top element of the stack
	 * @return Object on the top of the stack
	 * @throws hr.fer.zemris.java.custom.collections.EmptyStackException
	 */
	
	public Object pop() throws hr.fer.zemris.java.custom.collections.EmptyStackException {
		if ( isEmpty() ) throw new hr.fer.zemris.java.custom.collections.EmptyStackException();
		
		Object tmp = super.get(stackPointer - 1);
		super.remove(stackPointer - 1);
		stackPointer--;
		
		return tmp;
	}
	
	/**
	 * Returns the top element of the stack
	 * @return Object on the top of the stack
	 * @throws hr.fer.zemris.java.custom.collections.EmptyStackException
	 */
	
	public Object peek() throws hr.fer.zemris.java.custom.collections.EmptyStackException {
		if ( isEmpty() ) throw new hr.fer.zemris.java.custom.collections.EmptyStackException();
		
		return super.get(stackPointer - 1);
	}
	
	/**
	 * Clears the stack
	 */
	
	public void clear() {
		super.clear();
		stackPointer = 0;
	}
}
