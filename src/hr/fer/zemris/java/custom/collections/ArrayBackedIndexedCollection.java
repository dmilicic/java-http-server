package hr.fer.zemris.java.custom.collections;


/**
 * @author Dario Miličić
 * @version 1.0
 */
public class ArrayBackedIndexedCollection {
	
	
	private int size;
	private int capacity;
	private Object[] elements;
	
	/**
	 * Resize an array by reallocating its capacity by a factor of 2
	 * @param oldArray array to be resized
	 * @param oldCapacity old capacity of the old array
	 * @return returns a reallocated array that contains all the elements from the old array
	 */
	
	private Object[] resizeArray(Object[] oldArray, int oldCapacity) {
		
		int newCapacity = 2*oldCapacity;
		Object[] tmp = new Object[newCapacity];
		
		System.arraycopy(oldArray, 0, 
				tmp, 0, oldArray.length);
		capacity = newCapacity;
		
		return tmp;
	}
	
	/**
	 * Default constructor
	 */
	
	public ArrayBackedIndexedCollection() {
		capacity = 16;
		elements = new Object[16];
		size = 0;
	}
	
	/**
	 * Custom constructor with an argument initialCapacity
	 * @param initialCapacity initial capacity of the collections array
	 */
	
	public ArrayBackedIndexedCollection(int initialCapacity) throws IllegalArgumentException {
		if (initialCapacity < 1) throw new IllegalArgumentException();
		capacity = initialCapacity;
		elements = new Object[capacity];
		size = 0;
	}
	
	/**
	 * Checks if the array is empty
	 * @return returns true if the array is empty, returns false if the array has at least one element
	 */

	public boolean isEmpty() {
		if ( size == 0 )
			return true;
		return false;
	}
	
	/**
	 * Returns the size of the array
	 * @return size of the collection array
	 */
	
	public int size() {
		return size;
	}

	/**
	 * Adds an object to the array
	 * @param value value of the object to be added
	 * @throws NullPointerException value must not be null
	 */
	
	public void add(Object value) throws IllegalArgumentException {
		if (value == null) throw new IllegalArgumentException();
		
		if ( size >= capacity )
			elements = resizeArray(elements, capacity);
		elements[size++] = value;
	}
	
	/**
	 * Returns an object from the array on given index
	 * @param index index of object to be returned
	 * @return returns an object from the array
	 * @throws IndexOutOfBoundsException index must be between 0 and size-1, inclusive
	 */
	
	public Object get(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index > size - 1) throw new IndexOutOfBoundsException();
		return elements[index];
	}
	
	/**
	 * Removes an object from the array
	 * @param index index of object to be removed
	 * @throws IndexOutOfBoundsException index must be between 0 and size-1, inclusive
	 */
	
	public void remove(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index > size - 1) throw new IndexOutOfBoundsException();
		for(int i = index; i < elements.length - 1; i++) 
			elements[i] = elements[i+1];
		size--;
	}
	
	/**
	 * Inserts an object in the array
	 * @param value object value to be inserted
	 * @param position position to be inserted into
	 * @throws IndexOutOfBoundsException must be between 0 and size, inclusive
	 */
	
	public void insert(Object value, int position) throws IndexOutOfBoundsException, IllegalArgumentException {
		if ( position < 0 || position > size ) throw new IndexOutOfBoundsException();
		if ( value == null ) throw new IllegalArgumentException();
		
		if ( size >= capacity )
			elements = resizeArray(elements, capacity);
		
		for(int i = elements.length - 1; i > position; i--)
			elements[i] = elements[i-1];
		elements[position] = value;
		size++;
	}
	
	/**
	 * Searches for the value in the array and returns its index if it is found or -1 if it is not found
	 * @param value value to be searched
	 * @return index of argument value or -1 if no such value exists in the array
	 */
	
	public int indexOf(Object value) {
		for(int i = 0; i < elements.length; i++)
			if (elements[i].equals(value)) return i;
		return -1;
	}
	
	/**
	 * Searches for the value in the array
	 * @param value value to be searched
	 * @return returns true if the object is found or false if it is not found
	 */

	public boolean contains(Object value) {
		for(int i = 0; i < elements.length; i++)
			if (elements[i].equals(value)) return true;
		return false;
	}
	
	/**
	 * clears the array
	 */
	
	public void clear() {
		size = 0;
	}
}
