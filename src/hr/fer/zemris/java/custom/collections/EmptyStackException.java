package hr.fer.zemris.java.custom.collections;

/**
 * 
 * @author Dario Miličić
 *
 */

public class EmptyStackException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EmptyStackException() {
		super();
	}
	
	public EmptyStackException(String message) {
		super(message);
	}
	
	public EmptyStackException(String message, Throwable T) {
		super(message, T);
	}
}
