package hr.fer.zemris.java.custom.scripting.tokens;

public class TokenConstantInteger extends Token {

	private Integer value;
	
	public TokenConstantInteger(int value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}

	public String asText() {
		return Integer.toString((int) getValue());
	}

}
