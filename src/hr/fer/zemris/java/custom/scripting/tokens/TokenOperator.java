package hr.fer.zemris.java.custom.scripting.tokens;

public class TokenOperator extends Token {

	private String symbol;
	
	public TokenOperator(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public String asText() {
		return getSymbol();
	}

	@Override
	public String getValue() {
		return  symbol;
	}
}
