package hr.fer.zemris.java.custom.scripting.tokens;

public class TokenConstantDouble extends Token {
	
	private Double value;
	
	public TokenConstantDouble(double value) {
		this.value = value;
	}

	public Double getValue() {
		return value;
	}

	public String asText() {
		return Double.toString((double) getValue());
	}

}
