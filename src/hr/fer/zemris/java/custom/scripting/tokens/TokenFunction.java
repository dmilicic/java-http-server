package hr.fer.zemris.java.custom.scripting.tokens;

public class TokenFunction extends Token {

	private String name;
	
	public TokenFunction(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public String asText() {
		return getName();
	}

	@Override
	public String getValue() {
		return name;
	}
}
