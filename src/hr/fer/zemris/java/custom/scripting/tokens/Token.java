package hr.fer.zemris.java.custom.scripting.tokens;

public abstract class Token {
	
	protected TokenConstantInteger numInt;
	
	public Token() {
	}
	
	public abstract Object getValue();
	
	public String asText() {
		return "";
	}
}
