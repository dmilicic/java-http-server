/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

/**
 * A value wrapper class that contains a value of type Integer, Double, String or null.
 * @author Dario Miličić
 *
 */
public class ValueWrapper {

	private Object value;
	
	/**
	 * Default constructor. Accepts Objects of type Integer, Double, String or null as its parameter.
	 */
	public ValueWrapper(Object value) {
		
		if(!(value instanceof Integer ||
			 value instanceof Double  ||
		     value instanceof String  ||
		     value == null))
			throw new RuntimeException("Invalid type in input!");
		
		if(value == null)
			this.value = (Integer) 0;
		else
			this.value = value;
	}

	/**
	 * Gets the value of stored element
	 * @return returns the value of stored element
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Sets the value of stored element
	 * @param value value to be stored
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
	/**
	 * Checks if the object type is string or null and converts it appropriately. If the value is null then it is converted 
	 * to an integer of value 0. If the value is a string object then it parses it to double or integer, depending on the format of the string
	 * @param value value to be checked
	 * @return returns the value in an appropriate type depending on the parameter
	 */
	private Object typeCheck(Object value) {
			
		if(value == null)
			return 0;
		
		if(value instanceof String) {
			if( value.toString().contains(".") || value.toString().contains("E") ) {
				try {
					value = Double.parseDouble((String) value);
					return (Double) value;
				}
				catch (RuntimeException re) {
					System.out.println("Wrong input type for double!");
				}
			} else {
				try {
					value = Integer.parseInt( (String) value);
					return (Integer) value;
				}
				catch (RuntimeException re) {
					System.out.println("Wrong input type for integer!");
				}
			}
		}
		
		return value;
	}
	
	/**
	 * Increments the value of the stored element.
	 * @param incValue value to be added
	 */
	public void increment(Object incValue) {
		
		incValue = typeCheck(incValue);
		
		if( value instanceof Double && incValue instanceof Double ) {
			value = (Double) value + (Double) incValue;
		}
		else if( value instanceof Integer && incValue instanceof Double)	{

				value = Double.parseDouble(value.toString()) + (Double) incValue;
		}
		else if( value instanceof Double && incValue instanceof Integer ) {
				value = (Double) value + Double.parseDouble(incValue.toString());
		}
		else
			value = Integer.parseInt(value.toString()) 
			+ (Integer) incValue;
	}
	
	/**
	 * Decrements the value of the stored element
	 * @param decValue value to be subtracted
	 */
	public void decrement(Object decValue) {
		
		decValue = typeCheck(decValue);
		
		if( value instanceof Double && decValue instanceof Double ) {
			value = (Double) value - (Double) decValue;
		}
		else if( value instanceof Integer && decValue instanceof Double)	{

				value = Double.parseDouble(value.toString()) - (Double) decValue;
		}
		else if( value instanceof Double && decValue instanceof Integer ) {
				value = (Double) value - Double.parseDouble(decValue.toString());
		}
		else
			value = Integer.parseInt(value.toString()) - (Integer) decValue;
	}
	
	/**
	 * Multiplies the value of the stored element
	 * @param mulValue value to be multiplied with
	 */
	public void multiply(Object mulValue) {
		
		mulValue = typeCheck(mulValue);
		
		if( value instanceof Double && mulValue instanceof Double ) {
			value = (Double) value * (Double) mulValue;
		}
		else if( value instanceof Integer && mulValue instanceof Double)	{

				value = Double.parseDouble(value.toString()) * (Double) mulValue;
		}
		else if( value instanceof Double && mulValue instanceof Integer ) {
				value = (Double) value * Double.parseDouble(mulValue.toString());
		}
		else
			value = Integer.parseInt(value.toString()) * (Integer) mulValue;
	}
	
	/**
	 * Divides the value of the stored element
	 * @param divValue value to be divided with
	 */
	public void divide(Object divValue) {
		
		divValue = typeCheck(divValue);
		
		Object checkZero = divValue;
		Integer zeroInt = 1;
		Double zeroDouble = 1.0;
		
		try{
			zeroInt = Integer.parseInt(checkZero.toString());
			zeroDouble = Double.parseDouble(checkZero.toString());
		}
		catch (NumberFormatException n) {
		}
		
		if( zeroDouble == 0 || zeroInt == 0 ) throw new RuntimeException("Error! Dividing with zero!");
		
		if( value instanceof Double && divValue instanceof Double ) {
			value = (Double) value / (Double) divValue;
		}
		else if( value instanceof Integer && divValue instanceof Double)	{

				value = Double.parseDouble(value.toString()) / (Double) divValue;
		}
		else if( value instanceof Double && divValue instanceof Integer ) {
				value = (Double) value / Double.parseDouble(divValue.toString());
		}
		else
			value = Integer.parseInt(value.toString()) / (Integer) divValue;
	}
	
	/**
	 * Compares the value of the stored element.
	 * @param withValue value to be compared with
	 * @return returns 1 if the stored value is greater, returns 0 if the values are equal, returns -1 if the parameter value is greater than the stored value
	 */
	public int numCompare(Object withValue) {
		
		withValue = typeCheck(withValue);
		value = typeCheck(value);
		
		withValue = Double.parseDouble(withValue.toString());
		Double tmpValue = Double.parseDouble(value.toString());
		
		if( (Double) tmpValue > (Double) withValue )
			return 1;
		else if ( Math.abs( (Double) tmpValue - (Double) withValue) < 1E-9  )
			return 0;
		return -1;

	}	
}
