package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;
import hr.fer.zemris.java.webserver.RequestContext;

public class SmartScriptEngine {
	private DocumentNode documentNode;
	private RequestContext requestContext;
	private ObjectMultistack multistack = new ObjectMultistack();
	
	
	private INodeVisitor visitor = new INodeVisitor() {
		
		@Override
		public void visitTextNode(TextNode node) {
			try {
				requestContext.write(node.getText());
			} catch (IOException e) {
				System.err.print("I/O error has occured!");
				System.exit(-1);
			}
		}
		
		@Override
		public void visitForLoopNode(ForLoopNode node) {
			
			TokenVariable var = node.getVariable();
			Token start = node.getStartExpression();
			Token end = node.getEndExpression();
			Token step = node.getStepExpression();
			
			if( step == null ) {
				step = new TokenConstantInteger(1);
			}
			
			multistack.push(var.asText(), new ValueWrapper(start.getValue()));

			while(multistack.peek(var.asText()).numCompare(end.getValue()) != 1) {
				
				int num = node.numberOfChildren();
				for (int i = 0; i < num; i++) {
					node.getChild(i).accept(this);
				}
				
				multistack.peek(var.asText()).increment(step.getValue());
			}
			
			multistack.pop(var.asText());
		}
		
		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<ValueWrapper> stack = new Stack<>();
			Token[] tokens = node.getTokens();
			
			for (int i = 0; i < tokens.length; i++) {
				
				if(tokens[i] instanceof TokenConstantDouble || tokens[i] instanceof TokenConstantInteger || tokens[i] instanceof TokenString) {
					stack.push(new ValueWrapper(tokens[i].getValue()));
				} else if(tokens[i] instanceof TokenVariable) {
					ValueWrapper value = new ValueWrapper(multistack.data.get(tokens[i].asText()).getValue().getValue());
					stack.push(value);
				} else if(tokens[i] instanceof TokenOperator) {
					
					ValueWrapper operand1 = stack.pop();
					ValueWrapper operand2 = stack.pop();
					
					switch (tokens[i].asText()) {
					case "+":
						operand1.increment(operand2.getValue());
						break;
					case "-":
						operand1.decrement(operand2.getValue());
						break;
					case "*":
						operand1.multiply(operand2.getValue());
						break;
					case "/":
						operand1.divide(operand2.getValue());
						break;
					}
					
					stack.push(operand1);
					
				} else if(tokens[i] instanceof TokenFunction) {
					switch (tokens[i].asText()) {
					case "@sin":
						sin(stack);
						break;
					case "@decfmt":
						decfmt(stack);
						break;
					case "@dup":
						dup(stack);
						break;
					case "@setMimeType":
						setMimeType(stack);
						break;
					case "@paramGet":
						paramGet(stack);	
						break;
					case "@pparamGet":
						pparamGet(stack);
						break;
					case "@pparamSet":
						pparamSet(stack);
						break;
					default:
						throw new SmartScriptParserException();
					}
				}
			}
			
			for(ValueWrapper e : stack) {
				try {
					requestContext.write(e.getValue().toString());
				} catch (IOException e1) {
					System.err.println("I/O error has occured!");
					System.exit(-1);
				}
			}
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			int num = node.numberOfChildren();
			for (int i = 0; i < num; i++) {
				node.getChild(i).accept(this);
			}
		}
		
		private void sin(Stack<ValueWrapper> stack) {
			stack.push(new ValueWrapper(
					Math.sin(
							Double.parseDouble(
									stack.pop().getValue().toString()
									)
							)
					)
			);
		}
		
		private void decfmt(Stack<ValueWrapper> stack) {
			String format = (String) stack.pop().getValue();
			DecimalFormat f = new DecimalFormat(format);
			stack.push(new ValueWrapper(
					f.format(stack.pop().getValue())));
		}

		private void dup(Stack<ValueWrapper> stack) {
			ValueWrapper value = stack.pop();
			stack.push(value);
			stack.push(value);
		}
		
		private void setMimeType(Stack<ValueWrapper> stack) {
			requestContext.setMimeType((String)stack.pop().getValue());
		}
		
		private void paramGet(Stack<ValueWrapper> stack) {
			ValueWrapper dv = stack.pop();
			ValueWrapper name = stack.pop();
			String value = requestContext.getParameter(name.getValue().toString());
			stack.push(new ValueWrapper(value == null ? dv.getValue().toString() : value));
		}
		
		private void pparamGet(Stack<ValueWrapper> stack) {
			ValueWrapper dv = stack.pop();
			ValueWrapper name = stack.pop();
			String value = requestContext.getPersistentParameter(name.getValue().toString());
			stack.push(new ValueWrapper(value == null ? dv.getValue().toString() : value));
		}
		
		private void pparamSet(Stack<ValueWrapper> stack) {
			ValueWrapper name = stack.pop();
			ValueWrapper value = stack.pop();
			requestContext.setPersistentParameter(name.getValue().toString(), value.getValue().toString());
		}
	};
	
	public SmartScriptEngine(DocumentNode documentNode,
			RequestContext requestContext) {
		super();
		this.documentNode = documentNode;
		this.requestContext = requestContext;
	}

	public void execute() {
		documentNode.accept(visitor);
	}
}
