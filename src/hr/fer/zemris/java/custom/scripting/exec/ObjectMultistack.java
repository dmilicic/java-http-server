/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of a multiple stack collection on Object types
 * @author Dario Miličić
 *
 */
public class ObjectMultistack {

	Map<String, MultistackEntry> data;
	
	/**
	 * A stack implemented with single-linked lists that contains ValueWrapper element.
	 * @author Dario Miličić
	 */
	public class MultistackEntry {
		private ValueWrapper value;
		private MultistackEntry last;
		
		/**
		 * Default constructor
		 * @param value
		 */
		public MultistackEntry(ValueWrapper value) {
			this.value = value;
			last = null;
		}

		/**
		 * Gets the value of the Multistack entry
		 * @return returns a value from the top of the ObjectMultistack
		 */
		public ValueWrapper getValue() {
			return value;
		}
	}
	
	/**
	 * Default constructor 
	 */
	public ObjectMultistack() {
		data = new HashMap<String, ObjectMultistack.MultistackEntry>();
	}
	
	/**
	 * Puts a value in a map depending on its key. If the key is already contained within the map then the value is stored on a stack of values for that key. 
	 * If the key is not contained then a new key-value element is created.
	 * @param name value is added on this key
	 * @param valueWrapper value to be added
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		
		MultistackEntry entry = new MultistackEntry(valueWrapper);
		
		if(data.containsKey(name)) {
			entry.last = data.get(name);
			data.put(name, entry);
		} else {
			data.put(name, entry);
		}
	}
	
	/**
	 * Pops a value from the value stack if the key is contained and if the stack has elements.
	 * @param name key name for which the value stack will pop an element
	 * @return returns the element popped from the value stack
	 */
	public ValueWrapper pop(String name) {
		
		if(isEmpty(name)) throw new EmptyStackException();
		
		if(data.containsKey(name)) {
			MultistackEntry entry = data.get(name);
			data.put(name, entry.last);
			return entry.value;
		}
		else throw new IllegalArgumentException();
	}
	
	/**
	 * Peek on an element on the value stack depending on its key.
	 * @param name key name for which the value stack will peek
	 * @return returns the element on top of the value stack
	 */
	public ValueWrapper peek(String name) {
		
		if(isEmpty(name)) throw new EmptyStackException();
		
		if(data.containsKey(name)) {
			return data.get(name).value;
		}
		else throw new IllegalArgumentException();
	}
	
	/**
	 * Checks if the value stack is empty for a given key.
	 * @param name key name for which the value stack is checked
	 * @return returns true if the stack is empty or false if it is not
	 */
	public boolean isEmpty(String name) {
		if(data.containsKey(name)) {
			if(data.get(name) == null) return true;
			return false;
		}
		else throw new IllegalArgumentException();
	}
}
