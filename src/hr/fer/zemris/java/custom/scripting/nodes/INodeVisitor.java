package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * A visitor interface for executing smart script code
 * @author Dario Miličić
 *
 */
public interface INodeVisitor {
	
	/**
	 * Prints the value of the text node
	 */
	public void visitTextNode(TextNode node);
	
	/**
	 * Prints the syntax of a for loop node and calls visitors on all of its children
	 */
	public void visitForLoopNode(ForLoopNode node);
	
	/**
	 * Prints all of the tokens in this echo node in EchoNode syntax: [$ tokens $]
	 */
	public void visitEchoNode(EchoNode node);
	
	/**
	 * Calls the visitors for all of the children of this document node
	 */
	public void visitDocumentNode(DocumentNode node);	
}
