package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.Token;

public class EchoNode extends Node {

	private Token[] tokens;
	
	public EchoNode(Token[] tokens) {
		
		this.tokens = new Token[tokens.length];
		
		for(int i = 0; i < tokens.length; i++) {
			this.tokens[i] = tokens[i];
		}
	}

	public Token[] getTokens() {
		return tokens;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}
}
