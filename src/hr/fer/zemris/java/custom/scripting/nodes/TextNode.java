package hr.fer.zemris.java.custom.scripting.nodes;

public class TextNode extends Node {

	private String text;
	
	/**
	 * Default constructor that initiates its private variable to argument text
	 * @param text Initial value
	 */
	
	public TextNode(String text) {
		this.text = text;
	}
	
	/**
	 * Getter function for text
	 * @return returns text
	 */
	
	public String getText() {
		return text;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitTextNode(this);
	}
}
