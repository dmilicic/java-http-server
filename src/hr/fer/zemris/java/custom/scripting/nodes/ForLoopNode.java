package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.*;

public class ForLoopNode extends Node {
	
	private TokenVariable variable;
	private Token startExpression;
	private Token endExpression;
	private Token stepExpression;

	public ForLoopNode(TokenVariable variable, Token startExpression, Token endExpression, Token stepExpression) {
		
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	public TokenVariable getVariable() {
		return variable;
	}

	public Token getStartExpression() {
		
		return startExpression;
	}

	public Token getEndExpression() {
		return endExpression;
	}

	public Token getStepExpression() {
		return stepExpression;
	}

	public String getStartObject() {
		
		Token help = getStartExpression();
		
		if(help instanceof TokenConstantInteger) {
			int tmp = ((TokenConstantInteger) help).getValue(); 
			return Integer.toString(tmp);
		}
		if(help instanceof TokenConstantDouble) {
			double tmp = ((TokenConstantDouble) help).getValue();
			return Double.toString(tmp);
		}
		if(help instanceof TokenVariable) {
			return ((TokenVariable) help).getName();
		}
		return "";
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
	}
}
