package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;

public abstract class Node {

	private ArrayBackedIndexedCollection children;
	boolean cntChild = false;
	
	public abstract void accept(INodeVisitor visitor);
	
	/**
	 * Adds a child node 
	 * @param child Child node to be added
	 */
	
	public void addChildNode(Node child) {
		if(cntChild == false) {
			children = new ArrayBackedIndexedCollection();
			cntChild = true;
		}
		children.add(child);
	}
	
	/**
	 * Returns the number of children of a given node
	 * @return Number of children
	 */
	
	public int numberOfChildren() {
		if(children == null)
			return 0;
		return children.size();
	}
	
	/**
	 * Returns a child object from a child collection.
	 * @param index Index of a child to be returned.
	 * @return Returned child.
	 */
	
	public Node getChild(int index) {
		return (Node) children.get(index);
	}
	
	
}
