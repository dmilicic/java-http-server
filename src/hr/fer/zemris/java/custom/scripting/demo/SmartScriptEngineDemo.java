package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmartScriptEngineDemo {
	
	public static void demo1() {
		String documentBody = readFromDisk("osnovni.smscr");
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		
		// create engine and execute it
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
							  new RequestContext(System.out, parameters, persistentParameters, cookies)
			).execute();
	}
	
	public static void demo2() {
		String documentBody = readFromDisk("zbrajanje.smscr");
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		
		parameters.put("a", "4");
		parameters.put("b", "2");
		
		// create engine and execute it
		new SmartScriptEngine(
				new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)
		).execute();
	}
	
	public static void demo3() {
		String documentBody = readFromDisk("brojPoziva.smscr");
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		persistentParameters.put("brojPoziva", "3");
		
		RequestContext rc = new RequestContext(System.out, parameters, persistentParameters, cookies);
		new SmartScriptEngine(
				new SmartScriptParser(documentBody).getDocumentNode(), rc
		).execute();
		System.out.println("\nVrijednost u mapi: "+rc.getPersistentParameter("brojPoziva"));
	}
	
	
	public static void main(String[] args) {
		
		demo1();
		//demo2();
		//demo3();
	}

	private static String readFromDisk(String string) {
		
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(string));
		} catch (FileNotFoundException e) {
			System.err.println("File not found!");
			System.exit(-1);
		}
		
		char[] cbuf = new char[1024];
		StringBuilder sb = new StringBuilder();
		
		while(true) {
			int end = 0;
			try {
				end = reader.read(cbuf);
			} catch (IOException e) {
				System.err.println("I/O error has occured!");
				System.exit(-1);;
			}
			if(end == -1) break;
		
			sb.append(cbuf);
		}
		
		return sb.toString();
	}
}
