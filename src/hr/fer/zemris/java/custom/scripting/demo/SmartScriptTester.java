package hr.fer.zemris.java.custom.scripting.demo;


import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.*;
import hr.fer.zemris.java.custom.scripting.tokens.Token;

public class SmartScriptTester {

	
	/**
	 * Generates a string from a tree structure document
	 * @param document Starting node 
	 * @param docText Initial value of the generated string is "".
	 * @return Returns the generated string
	 */
	
	public static String createOriginalDocumentBody(Node document, String docText) {
		
		if(document == null)
			return docText;
		
		for(int i = 0; i < document.numberOfChildren(); i++) {
			Node tmp = (Node) document.getChild(i);
			
			if(tmp == null)
				continue;
			
			if(tmp instanceof TextNode) {
				TextNode tmpNode = (TextNode) tmp;
				docText += tmpNode.getText();
			}
			
			if(tmp instanceof ForLoopNode) {
 				ForLoopNode tmpNode = (ForLoopNode) tmp;
 				docText += "[$ FOR " + tmpNode.getVariable().getName() + " " +  tmpNode.getStartExpression().asText() + " " + tmpNode.getEndExpression().asText() + " ";
 				if (tmpNode.getStepExpression() != null) {
 					docText += tmpNode.getStepExpression().asText();
 				}
 				docText += "$]";
			}
			
			if(tmp instanceof EchoNode){
				EchoNode tmpNode = (EchoNode) tmp;
				Token[] help = tmpNode.getTokens();
				
				docText += "[= ";
				
				for( int j = 0; j < help.length; j++ ) {
					docText += help[j].asText() + " ";
				}
				
				docText += "$]";
			}
			
			docText = createOriginalDocumentBody(tmp, docText);
			
			if(tmp instanceof ForLoopNode) {
				docText += "[$ END $]";
			}
		}
		return docText;
	}
	
	public static void main(String[] args) {
		String docBody = "This is   sample text. \\[$=1$]\n [$    FOR    i  -1 10 1$]\n This is [$= i $]-th time this message is generated.\n [$END$]\n [$FOR i     0.5 10 $]\n"+"sin([$=i$]^2) = [$= i i * @sin \"0.000[$ $]\" \" \"  @decfmt$]\n [$END$]\n";
		//String docBody = "Example \\[$=1$]. Now actually write one [$=1$] isus dwald";
		//String docBody = "Lala [$ FOR Lala 1.9 2.0 5$]\r\nTsk[$ = No \"goog\\\"\"\"\" 13 + @wa_ga  $] \\[$END$][$FOR ni -1.0 2 $][$END$][$END$]";
		//String docBody = "[$ FOR Lala 1.9 2.0 5$][$ = No \"goog\\\"\"\" 13 + @wa_ga \"\\\"\"$][$END$]";
		//String docBody = "[$= i i * @sin \"0.000[$ $]\" \" \"  @decfmt $]";
		//String docBody = "[$ = No \"goog\\\"\"\"\" 13 + @wa_ga  $]";
		//String docBody = "[$ = \"$]";
		
		
		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(docBody);
		} catch(SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch(Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = createOriginalDocumentBody(document, "");
		System.out.println(originalDocumentBody);
	}

}
