package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.tokens.Token;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * A visitor implementation for executing smart script code
 * @author Dario Miličić
 *
 */

public class TreeWriter {
	
	public static class WriterVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.getText());
		}


		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print("[$ FOR " + node.getStartExpression().asText() + " " + node.getEndExpression().asText());
			if( node.getStepExpression() != null ) {
				System.out.print(" " + node.getStepExpression().asText());
			}
			System.out.print(" $]");
			
			int num = node.numberOfChildren();
			
			for (int i = 0; i < num; i++) {
				node.getChild(i).accept(this);
			}
			
			System.out.print("[$ END $]");
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			Token[] tokens = node.getTokens();
			
			System.out.print("[$= ");
			for (int i = 0; i < tokens.length; i++) {
				System.out.print(tokens[i].asText() + " ");
			}
			System.out.print("$]");
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			int num = node.numberOfChildren();
			
			for (int i = 0; i < num; i++) {
				node.getChild(i).accept(this);
			}
		}
		
	}
	
	public static void main(String[] args) {
		
		if( args.length < 1 ) {
			System.err.println("Error! File path expected!");
			System.exit(-1);
		}
		
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(args[0]));
		} catch (FileNotFoundException e) {
			System.err.println("File not found!");
			System.exit(-1);
		}
		
		char[] cbuf = new char[1024];
		StringBuilder sb = new StringBuilder();
		
		while(true) {
			int end = 0;
			try {
				end = reader.read(cbuf);
			} catch (IOException e) {
				System.err.println("I/O error has occured!");
				System.exit(-1);;
			}
			if(end == -1) break;
		
			sb.append(cbuf);
		}
		
		String docBody = sb.toString();
		
		SmartScriptParser p = new SmartScriptParser(docBody);
		p.getDocumentNode().accept(new WriterVisitor());
		//System.out.println(docBody);
	}
}
